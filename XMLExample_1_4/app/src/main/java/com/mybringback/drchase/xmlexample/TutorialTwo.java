package com.mybringback.drchase.xmlexample;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by Dr.Chase on 2016.11.14..
 */

public class TutorialTwo extends ListActivity {

    String classNames[] = {"MainActivity", "Menu", "Sweet", "TutorialOne"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, classNames));
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    protected void onListItemClick(ListView lv, View v, int position, long id)  {
        super.onListItemClick(lv,v,position,id);
        String openClass = classNames[position];
        try {
            Class selected = Class.forName("com.mybringback.drchase.xmlexample." + openClass);
            Intent selectedIntent = new Intent(this, selected);
            startActivity(selectedIntent);
        }
        catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (Exception e)   {

        }
    }

}
