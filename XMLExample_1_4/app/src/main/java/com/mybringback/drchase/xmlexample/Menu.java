package com.mybringback.drchase.xmlexample;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Dr.Chase on 2016.11.06..
 */

public class Menu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Button Sound
        final MediaPlayer buttonSound = MediaPlayer.create(Menu.this, R.raw.button_click);

        // Setting up the button references
        Button tut1 = (Button) findViewById(R.id.tutorial1);
        Button tut2 = (Button) findViewById(R.id.tutorial2);
        Button btnWallpaper = (Button) findViewById(R.id.tutorial3);
        Button toCanvas = (Button) findViewById(R.id.tutorial4);
        Button surfaceView = (Button) findViewById(R.id.tutorial5);

        tut1.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View v) {

                startActivity( new Intent("com.mybringback.drchase.xmlexample.TUTORIALONE"));
            }
        });

        tut2.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View v) {
                buttonSound.start();
                startActivity( new Intent("com.mybringback.drchase.xmlexample.TUTORIALTWO"));
            }
        });

        btnWallpaper.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View v) {
                buttonSound.start();
                startActivity( new Intent("com.mybringback.drchase.xmlexample.TUTORIALTHREE"));
            }
        });

        toCanvas.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View v) {
                buttonSound.start();
                startActivity( new Intent("com.mybringback.drchase.xmlexample.TUTORIALFOUR"));
            }
        });

        surfaceView.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View v) {
                buttonSound.start();
                startActivity( new Intent("com.mybringback.drchase.xmlexample.SURFACEVIEWEXAMPLE"));
            }
        });
    }

    @Override
    protected void onPause()    {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu)   {
        super.onCreateOptionsMenu(menu);
        MenuInflater awesome = getMenuInflater();
        awesome.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())   {
            case R.id.menuSweet:
                startActivity(new Intent("com.mybringback.drchase.xmlexample.SWEET"));
                return true;
            case R.id.menuToast:
                // Setup in next tutorial
                Toast andEggs = Toast.makeText(Menu.this, "This is a toast", Toast.LENGTH_SHORT);
                andEggs.show();
                return true;
        }

        return false;
    }
}
