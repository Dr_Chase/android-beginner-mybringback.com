package com.mybringback.drchase.xmlexample;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created by Dr.Chase on 2016.11.15..
 */

public class Sprite {

    int x, y;
    int xSpeed, ySpeed;
    int height, width;
    Bitmap b;
    SurfaceViewExample.OurView ov;

    // for sprite animation
    int currentFrame = 0;
    int direction = 1;

    public Sprite(SurfaceViewExample.OurView ourView, Bitmap bat) {
        b = bat;
        ov = ourView;

        // 4 rows
        height = b.getHeight() / 4;
        // 4 columns
        width = b. getWidth() / 4;
        x = y = 0;
        xSpeed = 5;
        ySpeed = 0;
    }

    public void onDraw(Canvas canvas) {

        update();
        int srcY = direction * height;
        int srcX = currentFrame * width;
        Rect src = new Rect(srcX, srcY, srcX + width, srcY +height);
        // scaling factor
        Rect dst = new Rect (x, y, x+ width , y+ height );
        canvas.drawBitmap(b, src, dst, null);


    }

    private void update()   {


        // 0 = down
        // 1 = right
        // 2 = up
        // 3 = left

        // facing down
        if ( x > ov.getWidth() - width -xSpeed  )   {
            xSpeed = 0;
            ySpeed = 5;
            direction = 0;
        }

        //facing left
        if ( y > ov.getHeight() - height -ySpeed  )   {
            xSpeed = -5;
            ySpeed = 0;
            direction = 3;
        }

        // facing up
        if( x + xSpeed < 0) {
            x = 0;
            xSpeed = 0;
            ySpeed = -5;
            direction = 2;
        }

        //facing right
        if ( y + ySpeed < 0)    {
            y = 0;
            xSpeed = 5;
            ySpeed = 0;
            direction = 1;
        }

        currentFrame = ++currentFrame % 4;
        x = x + xSpeed;
        y = y + ySpeed;

        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
