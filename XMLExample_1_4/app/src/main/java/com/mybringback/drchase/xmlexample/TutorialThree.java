package com.mybringback.drchase.xmlexample;

//import android.support.v7.app.Activity;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Dr.Chase on 2016.11.14..
 */

public class TutorialThree extends AppCompatActivity implements View.OnClickListener {

    ImageView display;
    int toPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN , WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.wallpaper);

        toPhone = R.drawable.background_apple_1492935;

        display = (ImageView) findViewById(R.id.ivDisplay);
        ImageView image1 = (ImageView) findViewById( R.id.ivImage1);
        ImageView image2 = (ImageView) findViewById( R.id.ivImage2);
        ImageView image3 = (ImageView) findViewById( R.id.ivImage3);
        ImageView image4 = (ImageView) findViewById( R.id.ivImage4);
        ImageView image5 = (ImageView) findViewById( R.id.ivImage5);

        Button setWall = (Button) findViewById(R.id.BsetWallpaper);

        image1.setOnClickListener(this);
        image2.setOnClickListener(this);
        image3.setOnClickListener(this);
        image4.setOnClickListener(this);
        image5.setOnClickListener(this);

        setWall.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())  {
            case R.id.ivImage1:
                display.setImageResource(R.drawable.background_cheese_1329511);
                toPhone = R.drawable.background_cheese_1329511;
                break;
            case R.id.ivImage2:
                display.setImageResource(R.drawable.background_apple_1492935);
                toPhone = R.drawable.background_apple_1492935;
                break;
            case R.id.ivImage3:
                display.setImageResource(R.drawable.background_bacon);
                toPhone = R.drawable.background_bacon;
                break;
            case R.id.ivImage4:
                display.setImageResource(R.drawable.background_rainbow_flower_1528089);
                toPhone = R.drawable.background_rainbow_flower_1528089;
                break;
            case R.id.ivImage5:
                display.setImageResource(R.drawable.skull_02_background_1493295);
                toPhone = R.drawable.skull_02_background_1493295;
                break;
            case R.id.BsetWallpaper:
                //InputStream inpuStr = getResources().openRawResource(toPhone); // debrecated and shit
                //Bitmap walllpaper = BitmapFactory.decodeStream(inpuStr); // debrecated and shit
                WallpaperManager myWallpaperManager
                        = WallpaperManager.getInstance(getApplicationContext());
                try {
                    //getApplicationContext().setWallpaper(walllpaper); // debrecated and shit
                    myWallpaperManager.setResource(toPhone);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
