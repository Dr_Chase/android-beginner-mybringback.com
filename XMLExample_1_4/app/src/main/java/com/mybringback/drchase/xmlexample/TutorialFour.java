package com.mybringback.drchase.xmlexample;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Dr.Chase on 2016.11.14..
 */


public class TutorialFour extends AppCompatActivity {

    DrawingTheBall ball;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ball = new DrawingTheBall(this);
        setContentView(ball);
    }
}
