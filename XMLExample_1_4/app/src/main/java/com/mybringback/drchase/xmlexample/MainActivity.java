package com.mybringback.drchase.xmlexample;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    MediaPlayer logoMusic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        final MediaPlayer logoMusic = MediaPlayer.create(MainActivity.this, R.raw.here);
        logoMusic.start();
        Thread logoTimer = new Thread() {
            public void run()   {
                try {
                    sleep(1);
                    logoMusic.stop();
                    logoMusic.release();
                    Intent menuIntent = new Intent("com.mybringback.drchase.xmlexample.MENU");
                    startActivity(menuIntent);

                }
                catch (Exception e)   {
                    Log.e("APP_TAG", "STACKTRACE");
                    Log.e("APP_TAG", Log.getStackTraceString(e));
                }
                finally {
                    finish();
                }
            }
        };
        logoTimer.start();
    }

    @Override
    protected void onPause()    {
        super.onPause();

        //logoMusic.release();
    }
}
