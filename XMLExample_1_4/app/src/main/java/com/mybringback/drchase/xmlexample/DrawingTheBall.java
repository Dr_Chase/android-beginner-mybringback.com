package com.mybringback.drchase.xmlexample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by Dr.Chase on 2016.11.14..
 */

public class DrawingTheBall extends View {

    Bitmap bball;
    int xPos, yPos;

    public DrawingTheBall(Context context) {
        super(context);

        bball = BitmapFactory.decodeResource(getResources(), R.drawable.fuckball);
        xPos = 0;
        yPos = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Rect ourRect = new Rect();
        ourRect.set( 0, 0, canvas.getWidth(), canvas.getHeight()/2 );

        Paint blue = new Paint();
        blue.setColor(Color.BLUE);
        blue.setStyle(Paint.Style.FILL);

        canvas.drawRect(ourRect, blue);

        if( xPos < canvas.getWidth())    {
            xPos += 8;
        }   else    {
            xPos = 0;
        }
        if( yPos < canvas.getHeight())    {
            yPos += 8;
        }   else    {
            yPos = 0;
        }
        canvas.drawBitmap(bball, xPos, yPos, new Paint());
        invalidate();
    }
}
