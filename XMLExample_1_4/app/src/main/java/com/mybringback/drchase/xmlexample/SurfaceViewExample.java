package com.mybringback.drchase.xmlexample;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/**
 * Created by Dr.Chase on 2016.11.14..
 */

public class SurfaceViewExample extends Activity implements View.OnTouchListener {

    OurView v;
    Bitmap ball, bat;
    float x, y;
    Sprite batSprite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        v = new OurView(this);
        v.setOnTouchListener(this);
        ball = BitmapFactory.decodeResource(getResources(), R.drawable.fuckball);
        bat = BitmapFactory.decodeResource(getResources(), R.drawable.bat_sprite_big);
        x = y = 0;
        setContentView(v);

    }

    @Override
    protected void onPause() {
        super.onPause();
        v.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        v.resume();
    }

    public class OurView extends SurfaceView implements Runnable {
        public OurView(Context context) {
            super(context);
            holder = getHolder();

        }

        Thread t = null;
        SurfaceHolder holder;
        boolean isItOK = false;


        @Override
        public void run() {

            batSprite = new Sprite(this, bat);
            while(isItOK == true)  {
                // perform canvas drawing
                if( !holder.getSurface().isValid() )    {
                    continue;
                }

                Canvas canvas = holder.lockCanvas();

                drawy(canvas);
                holder.unlockCanvasAndPost(canvas);

            }
        }

        protected void drawy(Canvas canvas)   {
            canvas.drawARGB( 255, 150, 150, 10);
            canvas.drawBitmap(ball, x - (ball.getWidth() / 2), y - (ball.getHeight() / 2), null);
            batSprite.onDraw(canvas);

        }

        public void pause() {
            isItOK = false;
            while(true) {
                try {
                    t.join();
                } catch(InterruptedException e )    {
                    e.printStackTrace();
                }
                break;
            }
        }

        public void resume()    {
            isItOK = true;
            t = new Thread (this);
            t.start();
        }
    }

    public boolean onTouch(View v, MotionEvent me) {

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        switch(me.getAction())  {
            case MotionEvent.ACTION_DOWN:
                x = me.getX();
                y = me.getY();
                break;
            case MotionEvent.ACTION_UP:
                x = me.getX();
                y = me.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                x = me.getX();
                y = me.getY();
                break;

        }

        return true;
    }

}
